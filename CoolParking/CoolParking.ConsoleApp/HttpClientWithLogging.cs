using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.ConsoleApp.Extensions;
using Newtonsoft.Json;

namespace CoolParking.ConsoleApp
{
    public static class HttpClientWithLogging
    {
        private static readonly HttpClient Client;
        private static readonly string BaseUrl = "http://localhost:5000/api";
        
        static HttpClientWithLogging()
        {
            var clientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true
            };
            Client = new HttpClient(clientHandler);
        }

        private static async Task LogResponse(HttpResponseMessage response)
        {
            var responseBody = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(responseBody);
            }
            else
            {
                var parsedResponseBody = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseBody);
                if (parsedResponseBody != null) Console.WriteLine(parsedResponseBody["Message"]);
            }
        }

        public static async Task GetAndLogResponse(string route)
        {
            try
            {
                var responseBody = await Client.GetStringAsync($"{BaseUrl}/{route}");
                Console.WriteLine(responseBody);
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");  
                Console.WriteLine("Message :{0} ",e.Message);
            }
        }
        
        public static async Task PostAndLogResponse(string route, object payload)
        {
            try
            {
                var response = await Client.PostAsync($"{BaseUrl}/{route}", payload.AsJson());
                await LogResponse(response);
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");  
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        
        public static async Task DeleteAndLogResponse(string route)
        {
            try
            {
                var response = await Client.DeleteAsync($"{BaseUrl}/{route}");
                await LogResponse(response);
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");  
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        
        public static async Task PutAndLogResponse(string route, object payload)
        {
            try
            {
                var response = await Client.PutAsync($"{BaseUrl}/{route}", payload.AsJson());
                await LogResponse(response);
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");  
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
    }
}