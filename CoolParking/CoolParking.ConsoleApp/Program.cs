﻿using System;
using System.Threading.Tasks;
using CoolParking.BL.Dtos;
using CoolParking.BL.Models;

namespace CoolParking.ConsoleApp
{
    public static class Program
    {
        static async Task Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine(@"Available options:
                1 - Print parking current balance
                2 - Print parking capacity
                3 - Print free parking spaces count
                4 - Print last transactions
                5 - Print transactions history
                6 - Print all vehicles on parking
                7 - Add vehicle
                8 - Remove vehicle
                9 - TopUp vehicle balance
                10 - exit
                ");
                
                var option = Console.ReadLine();

                switch (option)
                {
                    case "1":
                        await HttpClientWithLogging.GetAndLogResponse("parking/balance");
                        break;
                    case "2":
                        await HttpClientWithLogging.GetAndLogResponse("parking/capacity");
                        break;
                    case "3":
                        await HttpClientWithLogging.GetAndLogResponse("parking/freeplaces");
                        break;
                    case "4":
                        await HttpClientWithLogging.GetAndLogResponse("transactions/last");
                        break;
                    case "5":
                        await HttpClientWithLogging.GetAndLogResponse("transactions/all");
                        break;
                    case "6":
                        await HttpClientWithLogging.GetAndLogResponse("vehicles");
                        break;
                    case "7":
                    {
                        Console.WriteLine("Enter vehicle id");
                        var id = Console.ReadLine();
                        Console.WriteLine("Enter vehicle type: Bus/PassengerCar/Motorcycle/Truck");
                        var type = Console.ReadLine() ?? "";
                        Console.WriteLine("Enter vehicle balance");
                        var balance = Convert.ToInt32(Console.ReadLine() ?? "");

                        VehicleType vehicleType;
                        switch (type)
                        {
                            case "Bus":
                                vehicleType = VehicleType.Bus;
                                break;
                            case "PassengerCar":
                                vehicleType = VehicleType.PassengerCar;
                                break;
                            case "Motorcycle":
                                vehicleType = VehicleType.Motorcycle;
                                break;
                            case "Truck":
                                vehicleType = VehicleType.Truck;
                                break;
                            default:
                                Console.WriteLine("Entered invalid vehicle type");
                                continue;
                        }

                        var createVehicleDto = new CreateVehicleDto()
                        {
                            Id = id,
                            VehicleType = vehicleType,
                            Balance = balance
                        };
                        await HttpClientWithLogging.PostAndLogResponse("vehicles", createVehicleDto);
                        break;
                    }
                    case "8":
                    {
                        Console.WriteLine("Enter vehicle id");
                        var vehicleId = Console.ReadLine();
                    
                        await HttpClientWithLogging.DeleteAndLogResponse($"vehicles/{vehicleId}");
                        break;
                    }
                    case "9":
                    {
                        Console.WriteLine("Enter vehicle id");
                        var vehicleId = Console.ReadLine();
                    
                        Console.WriteLine("Enter amount");
                        var amount = Convert.ToDecimal(Console.ReadLine() ?? "");

                        var topUpVehicleDto = new TopUpVehicleDto()
                        {
                            Id = vehicleId,
                            Sum = amount
                        };
                        await HttpClientWithLogging.PutAndLogResponse("transactions/topUpVehicle", topUpVehicleDto);
                        break;
                    }
                    case "10":
                        return;
                }
            }
        }
    }
}