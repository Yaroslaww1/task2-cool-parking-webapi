using System;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace CoolParking.ConsoleApp.Extensions
{
    public static class AsJsonExtension
    {
        public static StringContent AsJson(this object o)
        {
            return new(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");
        }
    }
}

