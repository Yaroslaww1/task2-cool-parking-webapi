using System;

namespace CoolParking.BL.Exceptions
{
    public class ValidationBaseException : Exception
    {
        public ValidationBaseException(string subject): base($"{subject} is not valid")
        {
        }
    }
}