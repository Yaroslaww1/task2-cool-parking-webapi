using System;

namespace CoolParking.BL.Exceptions
{
    public class NotFoundBaseException : Exception
    {
        protected NotFoundBaseException(string subject): base($"{subject} not found")
        {
        }
    }
}