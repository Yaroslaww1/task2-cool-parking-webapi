namespace CoolParking.BL.Exceptions
{
    public class VehicleNotFoundException : NotFoundBaseException
    {
        public VehicleNotFoundException(): base("Vehicle")
        {
        }
    }
}