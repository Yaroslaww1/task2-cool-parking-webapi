namespace CoolParking.BL.Exceptions
{
    public class LogFileNotFoundException : NotFoundBaseException
    {
        public LogFileNotFoundException(): base("Log file")
        {
        }
    }
}