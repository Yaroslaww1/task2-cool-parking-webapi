﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;
using CoolParking.BL.Exceptions;
using CoolParking.BL.Helpers;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public readonly string Id;
        public readonly VehicleType VehicleType;
        public decimal Balance;

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            ValidateId(id);
            ValidateBalance(balance);
            
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static void ValidateId(string id)
        {
            var regex = new Regex("^.*-[0-9]+-.*$");
            // Only Ids like AA-0001-AA
            var isValid = regex.IsMatch(id) && id.Length == 10;

            if (!isValid)
            {
                throw new ValidationBaseException("Vehicle Id");
            }
        }

        private void ValidateBalance(decimal balance)
        {
            var isValid = balance >= 0;

            if (!isValid)
            {
                throw new ArgumentException("Vehicle Balance is not valid");
            }
        }

        public void TopUpBalance(decimal amount)
        {
            if (amount < 0)
            {
                throw new ArgumentException("TopUp balance cannot be negative");
            }

            Balance += amount;
        }

        private decimal WithdrawFromBalance(decimal amount)
        {
            decimal amountToWithdraw;
            switch (Balance)
            {
                case > 0 when Balance > amount:
                    amountToWithdraw = amount;
                    Balance -= amountToWithdraw;
                    return amountToWithdraw;
                case > 0 when Balance < amount:
                    amountToWithdraw = Balance + (amount - Balance) * Settings.PenaltyRate;
                    Balance -= amountToWithdraw;
                    return amountToWithdraw;
                case <= 0:
                    amountToWithdraw = amount * Settings.PenaltyRate;
                    Balance -= amountToWithdraw;
                    return amountToWithdraw;
            }

            return 0;
        }
        
        public decimal WithdrawRateFromBalance()
        {
            return VehicleType switch
            {
                VehicleType.Bus => WithdrawFromBalance(Settings.BusRate),
                VehicleType.Motorcycle => WithdrawFromBalance(Settings.MotorcycleRate),
                VehicleType.PassengerCar => WithdrawFromBalance(Settings.PassengerCarRate),
                VehicleType.Truck => WithdrawFromBalance(Settings.TruckRate),
                _ => 0
            };
        }
        
        static string GenerateRandomRegistrationPlateNumber()
        {
            return RandomUppercaseStringHelper.GenerateRandomUppercaseString(2) +
                "-" +
                RandomNumberStringHelper.GenerateRandomNumberString(4) +
                "-" +
                RandomUppercaseStringHelper.GenerateRandomUppercaseString(2);
        }

        public override string ToString()
        {
            return $"Id={Id} type={VehicleType} balance={Balance}";
        }
    }
}