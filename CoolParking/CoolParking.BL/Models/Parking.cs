﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoolParking.BL.Exceptions;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        public decimal Balance;
        public readonly int Capacity;
        
        public readonly Dictionary<string, Vehicle> Vehicles;
        public readonly List<TransactionInfo> Transactions;

        private static readonly Lazy<Parking> Lazy = new Lazy<Parking>(() => new Parking());
        
        private Parking()
        {
            Balance = Settings.StartingParkingBalance;
            Capacity = Settings.ParkingCapacity;
            
            Vehicles = new Dictionary<string, Vehicle>();
            Transactions = new List<TransactionInfo>();
        }
        
        public static Parking GetInstance => Lazy.Value;
        
        public int GetFreePlaces()
        {
            return Capacity - Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new(Vehicles.Values.ToList());
        }
        
        public Vehicle GetVehicleById(string id)
        {
            Vehicle.ValidateId(id);

            Vehicles.TryGetValue(id, out var vehicle);
            if (vehicle == null)
            {
                throw new VehicleNotFoundException();

            }
            return vehicle;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (Vehicles.Count == Capacity)
            {
                throw new InvalidOperationException("Cannot add a vehicle: parking is already full");
            }
            
            Vehicles.Add(vehicle.Id, vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = GetVehicleById(vehicleId);

            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Cannot remove a vehicle with a negative balance");
            }
            
            Vehicles.Remove(vehicleId);
        }

        public void TopUpVehicleBalance(string vehicleId, decimal sum)
        {
            var vehicle = GetVehicleById(vehicleId);

            vehicle.TopUpBalance(sum);
        }
        
        public void WithdrawRateFromVehicleBalance(string vehicleId)
        {
            var vehicle = Vehicles[vehicleId];

            var amount = vehicle.WithdrawRateFromBalance();
            Balance += amount;
            Transactions.Add(new TransactionInfo(vehicleId, amount));
        }

        public void Dispose()
        {
            Vehicles.Clear();
            Balance = 0;
            GC.SuppressFinalize(this);
        }
    }
}