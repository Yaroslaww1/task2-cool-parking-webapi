﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
namespace CoolParking.BL.Models
{
    public enum VehicleType : int
    {
        PassengerCar = 1,
        Truck = 2,
        Bus = 3,
        Motorcycle = 4
    }
}