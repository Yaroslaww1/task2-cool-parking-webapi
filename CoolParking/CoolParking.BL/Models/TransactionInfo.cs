﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public readonly DateTime DateTime;
        public readonly string VehicleId;
        public readonly decimal Sum;
        
        public TransactionInfo(string vehicleId, decimal sum)
        {
            DateTime = DateTime.Now;
            VehicleId = vehicleId;
            Sum = sum;
        }

        public override string ToString()
        {
            return $"{DateTime} vehicleId={VehicleId} amount={Sum}";
        }
    }
}