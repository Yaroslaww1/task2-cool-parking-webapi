﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class LogTimerService : TimerService, ILogTimerService
    {
        public LogTimerService() : base(Settings.TransactionLogWritePeriodInSeconds * 1000)
        {
       
        }
    }
}