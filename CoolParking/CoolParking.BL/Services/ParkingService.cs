﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Dtos;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public sealed class ParkingService : IParkingService
    {
        private readonly IWithdrawTimerService _withdrawTimer;
        private readonly ILogTimerService _logTimer;
        private readonly ILogService _logService;

        private readonly Parking _parking;
        
        public ParkingService(IWithdrawTimerService withdrawTimer, ILogTimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += _withdraw;
            _logTimer = logTimer;
            _logTimer.Elapsed += _saveTransactions;
            _logService = logService;

            _parking = Parking.GetInstance;
            
            _withdrawTimer.Start();
            _logTimer.Start();
        }
        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _parking.Dispose();
        }

        private void _withdraw(object o, ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.Vehicles.Values)
            {
                _parking.WithdrawRateFromVehicleBalance(vehicle.Id);
            }
        }

        private void _saveTransactions(object o, ElapsedEventArgs e)
        {
            _logService.Write(string.Join(",", _parking.Transactions.ToArray().Select(t => t.ToString()) ));
            _parking.Transactions.Clear();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }
        
        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.GetFreePlaces();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.GetVehicles();
        }
        
        public Vehicle GetVehicleById(string id)
        {
            return _parking.GetVehicleById(id);
        }
        
        public void AddVehicle(CreateVehicleDto createVehicleDto)
        {
            var newVehicle = new Vehicle(createVehicleDto.Id, createVehicleDto.VehicleType, createVehicleDto.Balance);
            _parking.AddVehicle(newVehicle);
        }
        
        public Vehicle AddVehicleAndReturn(CreateVehicleDto createVehicleDto)
        {
            var newVehicle = new Vehicle(createVehicleDto.Id, createVehicleDto.VehicleType, createVehicleDto.Balance);
            _parking.AddVehicle(newVehicle);
            return newVehicle;
        }

        public void RemoveVehicle(string vehicleId)
        {
            _parking.RemoveVehicle(vehicleId);
        }

        public void TopUpVehicle(TopUpVehicleDto topUpVehicleDto)
        {
            _parking.TopUpVehicleBalance(topUpVehicleDto.Id, topUpVehicleDto.Sum);
        }
        
        public Vehicle TopUpVehicleAndReturn(TopUpVehicleDto topUpVehicleDto)
        {
            TopUpVehicle(topUpVehicleDto);
            return GetVehicleById(topUpVehicleDto.Id);
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.Transactions.ToArray();
        }

        public decimal GetSumOfRecentTransactions()
        {
            return _parking.Transactions.Sum(t => t.Sum);
        }
        
        public string ReadFromLog()
        {
            return _logService.Read();
        }
    }
}