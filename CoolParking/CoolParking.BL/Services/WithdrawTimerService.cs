﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class WithdrawTimerService : TimerService, IWithdrawTimerService
    {
        public WithdrawTimerService() : base(Settings.WithdrawPeriodInSeconds * 1000)
        {
       
        }
    }
}