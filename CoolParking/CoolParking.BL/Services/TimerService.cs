﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Timers;
using CoolParking.BL.Interfaces;

public class TimerService : ITimerService
{
    public event ElapsedEventHandler Elapsed
    {
        remove => _timer.Elapsed -= value;
        add => _timer.Elapsed += value;
    }
    public double Interval { get; set; }
    private readonly Timer _timer;

    protected TimerService(double interval)
    {
        Interval = interval;

        _timer = new Timer(interval) {AutoReset = true};
    }

    public void Start()
    {
        _timer.Start();
    }

    public void Stop()
    {
        _timer.Stop();
    }

    public void Dispose()
    {
        _timer.Dispose();
    }
}