﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using CoolParking.BL.Exceptions;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath)
        {
            LogPath = logPath;
            
            InitializeLogFile();
        }

        private void InitializeLogFile()
        {
            if (!File.Exists(LogPath))
            {
                File.Create(LogPath).Dispose();
            }
        }
        
        public void Write(string logInfo)
        {
            using var logFile = new StreamWriter(LogPath, true);
            logFile.WriteLine(logInfo);
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new LogFileNotFoundException();
            }
            
            using var logFile = new StreamReader(LogPath);
            return logFile.ReadToEnd();
        }
    }
}