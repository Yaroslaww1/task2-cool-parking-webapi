namespace CoolParking.BL.Helpers
{
    public static class RandomUppercaseStringHelper
    {
        public static string GenerateRandomUppercaseString(int length)
        {
            var result = "";

            for (var i = 0; i < length; i++)
            {
                result += RandomUppercaseCharHelper.GenerateRandomUppercaseChar();
            }
            
            return result;
        }
    }
}