using System.ComponentModel.DataAnnotations;

namespace CoolParking.BL.Dtos
{
    public class TopUpVehicleDto
    {
        [Required]
        public string Id { get; set; }
        
        [Required]
        public decimal Sum { get; set; }
    }
}