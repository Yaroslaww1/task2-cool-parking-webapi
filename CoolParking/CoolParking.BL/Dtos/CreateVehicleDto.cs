using System.ComponentModel.DataAnnotations;
using CoolParking.BL.Models;

namespace CoolParking.BL.Dtos
{
    public class CreateVehicleDto
    {
        [Required]
        public string Id { get; set; }

        [Required]
        [EnumDataType(typeof(VehicleType))]
        public VehicleType VehicleType { get; set; }

        [Required]
        public decimal Balance { get; set; }
    }
}