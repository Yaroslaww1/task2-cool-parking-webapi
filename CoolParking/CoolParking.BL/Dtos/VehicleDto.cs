using System.ComponentModel.DataAnnotations;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.BL.Dtos
{
    public class VehicleDto
    {
        [Required]
        [JsonProperty("id")]
        public string Id { get; set; }

        [Required]
        [JsonProperty("vehicleType")]
        [EnumDataType(typeof(VehicleType))]
        public VehicleType VehicleType { get; set; }

        [Required]
        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }
}