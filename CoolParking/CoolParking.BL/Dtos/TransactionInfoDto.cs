using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace CoolParking.BL.Dtos
{
    public class TransactionInfoDto
    {
        [Required]
        [JsonProperty("transactionDate")]
        public DateTime DateTime { get; set; }
        
        [Required]
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        [Required]
        [JsonProperty("sum")]
        public decimal Sum { get; set; } = -1;
    }
}