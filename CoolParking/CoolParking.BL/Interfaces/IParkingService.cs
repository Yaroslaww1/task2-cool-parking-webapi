﻿using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Dtos;
using CoolParking.BL.Models;

namespace CoolParking.BL.Interfaces
{
    public interface IParkingService : IDisposable
    {
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
        ReadOnlyCollection<Vehicle> GetVehicles();
        void AddVehicle(CreateVehicleDto createVehicleDto);
        void RemoveVehicle(string vehicleId);
        void TopUpVehicle(TopUpVehicleDto topUpVehicleDto);
        TransactionInfo[] GetLastParkingTransactions();
        string ReadFromLog();
    }
}
