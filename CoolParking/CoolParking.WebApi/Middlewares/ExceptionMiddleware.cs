using System;
using System.Net;
using System.Threading.Tasks;
using CoolParking.BL.Dtos;
using CoolParking.BL.Exceptions;
using Microsoft.AspNetCore.Http;

namespace CoolParking.WebApi.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception e)
            {
                await HandleExceptionAsync(httpContext, e);
            }
        }
        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = exception switch
            {
                NotFoundBaseException _ => StatusCodes.Status404NotFound,
                ValidationBaseException _ => StatusCodes.Status400BadRequest,
                _ => 500
            };

            return context.Response.WriteAsync(new ErrorDetailsDto()
            {
                StatusCode = context.Response.StatusCode,
                Message = exception.Message,
                Detailed = exception.ToString()
            }.ToString());
        }
    }
}