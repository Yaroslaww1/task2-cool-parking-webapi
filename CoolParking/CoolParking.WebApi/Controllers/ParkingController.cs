using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;

namespace CoolParking.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        private readonly ParkingService _parkingService;

        public ParkingController(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            var balance = _parkingService.GetBalance();
            return Ok(balance);
        }
        
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            var capacity = _parkingService.GetCapacity();
            return Ok(capacity);
        }
        
        [HttpGet("freePlaces")]
        public ActionResult<decimal> GetFreePlaces()
        {
            var freePlaces = _parkingService.GetFreePlaces();
            return Ok(freePlaces);
        }
    }
}