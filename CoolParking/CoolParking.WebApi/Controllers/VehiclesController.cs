using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Dtos;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;

namespace CoolParking.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase

    {
        private readonly ParkingService _parkingService;

        public VehiclesController(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VehicleDto>> GetVehicles()
        {
            var vehicles = _parkingService.GetVehicles();
            return Ok(vehicles.Select(
                v => new VehicleDto{Id = v.Id, VehicleType = v.VehicleType,Balance = v.Balance}));
        }
        
        [HttpGet("{id}")]
        public ActionResult<VehicleDto> GetVehicle(string id)
        {
            var vehicle = _parkingService.GetVehicleById(id);
            return Ok(new VehicleDto{Id = vehicle.Id, VehicleType = vehicle.VehicleType,Balance = vehicle.Balance});
        }
        
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(VehicleDto))]
        public ActionResult<VehicleDto> CreateVehicle([FromBody] CreateVehicleDto createVehicleDto)
        {
            var newVehicle = _parkingService.AddVehicleAndReturn(createVehicleDto);
            return CreatedAtAction(nameof(CreateVehicle),
                new VehicleDto
                {
                    Id = newVehicle.Id,
                    VehicleType = newVehicle.VehicleType,
                    Balance = newVehicle.Balance
                });
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public ActionResult<VehicleDto> DeleteVehicle(string id)
        {
            _parkingService.RemoveVehicle(id);
            return NoContent();
        }
    }
}