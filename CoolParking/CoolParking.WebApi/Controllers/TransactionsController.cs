using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Dtos;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;

namespace CoolParking.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly ParkingService _parkingService;

        public TransactionsController(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        
        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfoDto>> GetLastTransactions()
        {
            var transactions = _parkingService.GetLastParkingTransactions();
            return Ok(transactions.Select(
                t => new TransactionInfoDto{DateTime = t.DateTime, Sum = t.Sum, VehicleId = t.VehicleId}));
        }
        
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            var transactions = _parkingService.ReadFromLog();
            return Ok(transactions);
        }
        
        [HttpPut("topUpVehicle")]
        public ActionResult<VehicleDto> TopUpVehicle([FromBody] TopUpVehicleDto topUpVehicleDto)
        {
            var vehicle = _parkingService.TopUpVehicleAndReturn(topUpVehicleDto);
            return Ok(new VehicleDto{Id = vehicle.Id, Balance = vehicle.Balance, VehicleType = vehicle.VehicleType});
        }
    }
}